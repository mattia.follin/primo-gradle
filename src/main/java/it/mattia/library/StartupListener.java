package it.mattia.library;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.awt.print.Book;
import java.util.List;
import java.util.logging.Logger;

@WebListener
public class StartupListener implements ServletContextListener{
   @Override
    public final void contextInitialized(
           final ServletContextEvent servletContextEvent){
       System.out.println("Hello World");
       _log.info("---> Application startup");
//       EntityManager = DatabaseManager.getEntityManager();
//
//       Query query = entityManager.createQuery("select t from Book t");
//
//       List<Book> bookList = query.getResultList();
//
//       for (Book book : bookList){
//           _log.info(book.toString());
//       }
//       _log.info("Number of books in library: " + bookList.size());
   }
   private static final Logger _log = Logger.getLogger(
           StartupListener.class.getName());
}
